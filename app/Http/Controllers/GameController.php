<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;

class GameController extends Controller
{
    public function create(){
        return view ('game.create');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ]);
        $game = new Game;

        $game->nama = $request->nama;
        $game->gameplay = $request->gameplay;
        $game->developer = $request->developer;
        $game->year = $request->year;

        $game->save();

        return redirect('/game');
    }

    public function index(){
        $game = Game::all();
        return view('game.index', compact('game'));
    }

    public function show($game_id){
        $game = Game::where('id', $game_id)->first();
        return view('game.show', compact('game'));
    }
    public function edit($game_id){
        $game = Game::where('id', $game_id)->first();
        return view('game.edit', compact('game'));
    }
    public function update(Request $request, $game_id){
        $request->validate([
            'nama' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ]);

        $game = Game::find($game_id);
 
        $game->nama = $request['nama'];
        $game->gameplay = $request['gameplay'];
        $game->developer = $request['developer'];
        $game->year = $request['year'];
        
        $game->save();

        return redirect ('/game');
    }
    public function destroy($game_id){
        $game = Game::find($game_id);
 
        $game->delete();
        return redirect('/game');
    }
}

