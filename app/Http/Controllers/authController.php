<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function register(){
        return view('halaman.register');
    }
    public function welcome(Request $request){
        $namadepan =$request['first_name'];
        $namabelakang=$request['last_name'];
        return view('halaman.welcome',compact ('namadepan','namabelakang'));

    }
    // public function welcome(){
    //     return view('halaman.welcome');
    // }
}
