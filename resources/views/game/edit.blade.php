<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Edit Data</title>

</head>

<body>

<h2>Edit Data Game</h2>

<form action="/game/{{$game->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label >Nama game</label>
      <input name="nama" type="text" value="{{$game->nama}}" class="form-control">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  
    <div class="form-group">
      <label>Gameplay</label>
      <textarea name="gameplay" class="form-control">{{$game->gameplay}}</textarea>
    </div>
    @error('gameplay')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  
    <div class="form-group">
      <label >Developer</label>
      <input name="developer" value="{{$game->developer}}" type="text" class="form-control">
    </div>
    @error('developer')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label >Year</label>
      <input name="year" type="number" value="{{$game->year}}" class="form-control">
    </div>
    @error('year')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>




<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>

