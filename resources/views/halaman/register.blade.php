@extends('layout.master')


@section('judul')
 halaman Register
@endsection

@section('content')
    <h2>Buat Account Baru</h2>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="get">
            <label>First name :</label><br><br>
                <input type="text" name="first_name"><br><br>
            <label>Last name :</label><br><br>
                <input type="text" name="last_name"><br><br>
            <label>Gender</label><br><br>
                <input type="radio" name ="gender">Male <br>
                <input type="radio" name ="gender">Female <br><br>
            <label>Nationality:</label><br><br>
            <select name="nationality">
                <option value="indo">Indonesia</option>
                <option value="usa">Amerika</option>
                <option value="ing">Inggris</option>
            </select><br><br>
            <label>Language Spoken</label><br><br>
                <input type="checkbox" name="bahasa">Bahasa Indonesia <br>
                <input type="checkbox" name="bahasa">English <br>
                <input type="checkbox" name="bahasa">Other <br><br>
            <label>Bio</label><br><br>
            <textarea name="message"  cols="30" rows="10"></textarea><br>
            <input type="submit" value ="Sign Up">
        </form>
@endsection