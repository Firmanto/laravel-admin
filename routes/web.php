<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','indexController@index');

Route::get('/register', 'authController@register');

Route::get('/welcome','authController@welcome');

Route::get('/data-table', function(){
    return view('table.data-table');
});

// CRUD cast
// create
Route::get('/cast/create', 'CastController@create');//Route menuju form create
Route::post('/cast','CastController@store');//route untuk menyimpan data ke DB

//read
Route::get('/cast', 'CastController@index');//route list cast
Route::get('/cast/{cast_id}', 'CastController@show');//route detail cast

Route::get('/cast/{cast_id}/edit', 'CastController@edit');//Route menuju form edit
Route::put('/cast/{cast_id}','CastController@update');//route untuk update data berdasarkan id

//delete
Route::delete('//cast/{cast_id}', 'CastController@destroy');//Route untuk hapus data di database


// CRUD game
// create
Route::get('/game/create', 'GameController@create');//Route menuju form create
Route::post('/game','GameController@store');//route untuk menyimpan data ke DB

//read
Route::get('/game', 'GameController@index');//route list game
Route::get('/game/{game_id}', 'GameController@show');//route detail game

Route::get('/game/{game_id}/edit', 'GameController@edit');//Route menuju form edit
Route::put('/game/{game_id}','GameController@update');//route untuk update data berdasarkan id

//delete
Route::delete('//game/{game_id}', 'GameController@destroy');//Route untuk hapus data di database